﻿using GeneralStoreAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Web.Http;

namespace GeneralStoreAPI.Controllers
{
    public class ProductController : ApiController
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        [HttpPost]
        public async Task<IHttpActionResult> Post(Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Products.Add(product);
                await _context.SaveChangesAsync();
                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetAll()
        {
            List<Product> listOfProducts = await _context.Products.ToListAsync();
            return Ok(listOfProducts);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetById([FromUri] int id)
        {
            Product foundProduct = await _context.Products.FindAsync(id);
            if (foundProduct == null)
            {
                return NotFound();
            }

            return Ok(foundProduct);
        }
        
        [HttpPut]
        public async Task<IHttpActionResult> Put([FromUri] int id, [FromBody] Product model)
        {
            if (ModelState.IsValid)
            {
                if (id != model.ID)
                {
                    return BadRequest("Product ID Mismatch");
                }
                _context.Entry(model).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {
            var foundProduct = await _context.Products.FindAsync(id);
            if (foundProduct == null)
            {
                return NotFound();
            }
            _context.Products.Remove(foundProduct);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
