﻿using IntroToAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IntroToAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient httpClient = new HttpClient();
            var response = httpClient.GetAsync("https://swapi.dev/api/people/1/").Result;

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Person personResponse = response.Content.ReadAsAsync<Person>().Result;
                Console.WriteLine(personResponse.name);

                foreach (string vehicleURL in personResponse.vehicles)
                {
                    HttpResponseMessage vehicleResponse = httpClient.GetAsync(vehicleURL).Result;
                    Console.WriteLine(vehicleResponse.Content.ReadAsStringAsync().Result);
                    Vehicle vehicle = vehicleResponse.Content.ReadAsAsync<Vehicle>().Result;
                    Console.WriteLine(vehicle.name);

                }
            }

            SwapiService SWAPIService = new SwapiService();

            for (int i = 0; i < 10; i++)
            {
                Person personOne = SWAPIService.GetPersonAsync($"https://swapi.dev/api/people/{i}").Result;


                if (personOne != null)
                {
                    Console.Clear();
                    Console.WriteLine($"The character that has been entered is: {personOne.name}");
                    foreach (string vehicleUrl in personOne.vehicles)
                    {
                        var vehicle = SWAPIService.GetVehicleAsync(vehicleUrl).Result;
                        Console.WriteLine($"They drive {vehicle.name}");
                    }

                    Console.ReadKey();
                }

            }

            var genericResponse = SWAPIService.GetAsyncGeneric<Vehicle>("https://swapi.dev/vehicles/4").Result;
            Console.WriteLine(genericResponse.cargo_capacity);
            Console.WriteLine(genericResponse.name);

            SearchResult<Person> skywalkers = SWAPIService.GetPersonSearchAsync("skywalker").Result;
            foreach(Person person in skywalkers.results)
            {
                Console.WriteLine(person.name);
            }
        }

    }
}
